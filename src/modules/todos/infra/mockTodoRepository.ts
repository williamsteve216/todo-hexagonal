import { IResponseTodos } from "../domain/entities";
import { ITodoRepository } from "../domain/ports/secondaries";

var jsonStringArrayOften = `
    {
        "results":[
            {
                "title":"Premier todo",
                "isDone":true
            }
        ]
    }
`;

var responseData: IResponseTodos = JSON.parse(jsonStringArrayOften);

export default class MockTodoRepository implements ITodoRepository {
	page?: number | undefined;
	results?: number | undefined;

	constructor(page: number = 1, results: number = 1) {
		(this.page = page), (this.results = results);
	}

	getTodos(): Promise<IResponseTodos> {
		return new Promise((resolve) => {
			resolve(responseData);
		});
	}
}
