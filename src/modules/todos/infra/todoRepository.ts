import { IResponseTodos } from "../domain/entities";
import { ITodoRepository } from "../domain/ports/secondaries";

export class TodoRepository implements ITodoRepository {
	page?: number | undefined;
	results?: number | undefined;

	constructor(page: number = 1, results: number = 1) {
		(this.page = page), (this.results = results);
	}

	async getTodos(): Promise<IResponseTodos> {
		let baseUrl: string = "https://randomuser.me/api/";
		let url: string;
		url = `${baseUrl}?page=${this.page}&results=${this.results}`;
		const res = await fetch(url);
		return res.json();
	}
}
