export interface IGetTodos {
	execute(page?: number, results?: number): (dispatch: any) => Promise<void>;
}
