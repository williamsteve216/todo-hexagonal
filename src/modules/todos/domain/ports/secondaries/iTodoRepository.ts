import { IResponseTodos } from "../../entities";

export interface ITodoRepository {
	page?: number;
	results?: number;
	getTodos(page?: number, results?: number): Promise<IResponseTodos>;
}
