import { ITodo } from "./todo";

export interface IResponseTodos {
	results: ITodo[];
}
