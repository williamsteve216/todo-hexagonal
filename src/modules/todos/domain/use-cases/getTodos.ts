import { IResponseTodos } from "../entities";
import { IGetTodos } from "../ports/primaries";
import { ITodoRepository } from "../ports/secondaries";
import { TodoActionTypes } from "./types";

export class GetTodos implements IGetTodos {
	private _todoRepository: ITodoRepository;
	constructor(todoRepository: ITodoRepository) {
		this._todoRepository = todoRepository;
	}
	execute(page?: number, results?: number): (dispatch: any) => Promise<void> {
		return async (dispatch) => {
			const { GET_TODOS_REQUEST, GET_TODOS_SUCCESS, GET_TODOS_FAIL } = TodoActionTypes;
			dispatch({
				type: GET_TODOS_REQUEST,
			});
			try {
				const todos: IResponseTodos = await this._todoRepository.getTodos(page, results);
				dispatch({
					type: GET_TODOS_SUCCESS,
					payload: todos.results,
				});
			} catch (error) {
				dispatch({
					type: GET_TODOS_FAIL,
					error,
				});
			}
		};
	}
}
