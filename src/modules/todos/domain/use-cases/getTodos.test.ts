import MockTodoRepository from "../../infra/mockTodoRepository";

const api = new MockTodoRepository();

describe("TodosUseCase", () => {
	it("should be defined", () => {
		expect(api.getTodos).toBeDefined();
	});
	it("should return a list of ten user", async () => {
		const result = await api.getTodos();
		let expected = result.results.length;
		expect(expected).toBeGreaterThan(0);
		expect(expected).toEqual(10);
	});
	it("should return that list with page 1", async () => {
		const result = await api.getTodos();
		let expected = result.results.length;
		expect(expected).toEqual(1);
	});
});
